import React, { useState } from 'react'
import OfficeAdmin from '../OfficeAdmin/officeadmin-index'
import OfficeNaNet from '../Officenanet/officenanet-index'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import * as yup from 'yup'
import './login-style.css'
import axios from 'axios'
//import DB from '../../db'


const Login = () => {
  
  const [checkLogin, setCheckLogin] = useState('login')  


// Function to mouse click to login

  const handleClickLogin = (values) => {
    axios.post('http://localhost:3001/login', {
      nomenanet: values.nomenanet,
      password: values.password,
    }).then((response) => {
      console.log (response.data)
      if (response.data.msg == 'admin') {
        setCheckLogin ('officeadmin')
      } else if (response.data.msg == 'user') {
        setCheckLogin ('officenanet')
      } else {
        console.log (response.data)
        alert ('User is not found')
      }
    })
  }


// Yup

  const validationLogin = yup.object().shape({
    nomenanet: yup
      .string()
      .required("Username is a required."),
    password: yup
      .string()
      .min(4, "The password don't 4 characters")
      .required("Password is a required.")
  })
  
  
// Route

  if (checkLogin == 'officenanet') {
    window.open ("http://localhost:3000?officenanet","_self")
    //window.open ("https://2023.catalogonanet.com/?officenanet","_self")
  } else if (checkLogin == 'officeadmin') {
    window.open ("http://localhost:3000?officeadmin","_self")
    //window.open ("https://2023.catalogonanet.com/?officeadmin","_self")
  }


//Send to Officenanet and OfficeAdmin
  const returnPage = () => {
    
    if (checkLogin == 'officenanet') {
      return ( 
        < OfficeNaNet />
      )
    } else if (checkLogin == 'officeadmin') { 
        return ( 
          < OfficeAdmin />
        )


// Login Screen

    } else if (checkLogin == 'login') {
        return (
          <div className = "loginContainer">
            <div className = "divContainer">
              <Formik
                initialValues={{
                  nomenanet: '',
                  password: '',
                }}
                onSubmit = {handleClickLogin}
                validationSchema = {validationLogin}
              >
                <Form>
                  
                  <p>
                    <Field type = "text" name = "nomenanet" className = "form-field" id = "nomenanet-id" placeholder = "Type your username here"></Field>
                    <ErrorMessage
                      component = "span"
                      name = "nomenanet"
                      className = "form-error"
                    />
                  </p>

                  <p>
                    <Field type = "password" name = "password" className = "form-field" id = "password-id" placeholder = "Type your password here"></Field>
                    <ErrorMessage
                      component = "span"
                      name = "password"
                      className = "form-error"
                    />
                  </p>

                  <p>
                    <button className = 'button' type = "submit">
                      Login
                    </button>
                  </p>

                </Form>
              </Formik>
            </div>
          </div>
        )
      }
  }

  return (
    <>
      {returnPage()}
    </>
  )
}

export default Login
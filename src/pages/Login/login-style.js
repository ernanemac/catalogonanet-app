import styled from "styled-components"

export const loginContainer = styled.div `
    height: 75vh;
    text-align: center;
`

export const divContainer = styled.div `
    display: inline-block;
    width: 300px;
    height: 200px;
    margin: auto;
    background-color: white;
    border-radius: 10px;
    box-shadow: 1px 1px 10px rgba(0, 0, 0, 0.3);
    margin-top: 100px;

    input {
        padding: 6px;
        width: 80%;
        margin-top: 10px;
        border-radius: 5px;
        border: 1px solid black;
    }

    .input-class {
        margin-top: 40px;
    }

    button {
        padding: 6px;
        width: 80%;
        margin-top: 20px;
        border-radius: 5px;
        border: 1px solid #e13b39;
        background-color: #e13b39;
        color: white;
    }

    button:hover {
        background-color: white;
        color: #e13b39;
        border: 1px solid #e13b39;
        translate: background .4s;
    }

`
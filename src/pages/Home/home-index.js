import React from 'react'
import Header from '../../components/Header/header-index'
import Main from '../../components/Main/main-index'
import Footer from '../../components/Footer/footer-index'
import GlobalStyle from '../../styles/global-style'

const Home = () => {
  return (
    <>
        < Header />
        < Main />
        < Footer />
        < GlobalStyle />
    </>
  )
}

export default Home
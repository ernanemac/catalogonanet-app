import styled from "styled-components"

export const OfficeAdminContainer = styled.div `
    height: 75vh;
    text-align: center;
`

export const h2Container = styled.div `
    display: inline-block;
    width: 300px;
    height: 50px;
    margin: auto;
    background-color: #e13b39;
    color: white;
    margin-top: 100px;

`

export const pContainer = styled.div `
    width: 300px;
    margin: auto;
    background-color: #e13b39;
    color: white;
    margin-right: 50px;
`
import React, { useState } from 'react'
import * as C from './officeadmin-style'
import axios from 'axios'


const OfficeAdmin = () => {

  const [nameUser, setNameUser] = useState('')

  axios.post("http://localhost:3001/officeadmin")
    .then((response) => {
      console.log (response)
      setNameUser(response.data)
  })

  return (
    <>
        <C.OfficeAdminContainer>
            <C.h2Container>
                <h2>Hi, {nameUser},</h2>
            </C.h2Container>
            <C.pContainer>
                <p>You're logged into OfficeAdmin!!!</p>
            </C.pContainer>
        </C.OfficeAdminContainer>

    </>
  )
}

export default OfficeAdmin
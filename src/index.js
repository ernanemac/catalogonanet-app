import React, {StrictMode} from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

ReactDOM.createRoot(document.querySelector ("#root")).render(
  <React.StrictMode>
  <BrowserRouter>
  <Routes>
      <Route path = "*" element = {<App/>}></Route>
    </Routes>
  </BrowserRouter>
  </React.StrictMode>
);



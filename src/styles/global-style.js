/* 
    cor01 - Red (background)
*/

import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle `
    
    :root {
        --cor01: #e13b39; 
    }

    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    body {
        background-color: var(--cor01);
        width: 100vw;
        height: 100vh;
        font-family: Arial;
    }
`

export default GlobalStyle
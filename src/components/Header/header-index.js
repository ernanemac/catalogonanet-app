import React from 'react'
import * as C from './header-style'
import ImgHeader from '../../img/header.png'

const Header = () => {
  return (
    <C.headerContainer>
        <img 
            src = {ImgHeader}
        />
    </C.headerContainer>
  )
}

export default Header
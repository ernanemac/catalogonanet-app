import styled from "styled-components";

export const mainContainer = styled.div `
    
    width: 150px;
    margin: auto;
    padding-top: 75px;
    height: 75vh;
    text-align: center;

    button {
        margin-top: 10px;
        padding: 10px;
        border-radius: 5px;
        border: 1px solid #e13b39;
    }

    button: hover {
        background-color: #e13b39;
        color: white;
        border: 1px solid white;
        translate: background .4s;
    }

`
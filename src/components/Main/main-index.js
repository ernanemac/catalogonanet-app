import React, { useEffect, useState } from 'react'
import ImgMainWhite from '../../img/officenanet-white.png'
import * as C from './main-style'
import LoginScreen from '../../pages/Login/login-index'
import OfficeAdmin from '../../pages/OfficeAdmin/officeadmin-index'
import OfficeNaNet from '../../pages/Officenanet/officenanet-index'

const Main = () => {

  const [loginPage, setLoginPage] = useState(0)


  // Step 3: Cut the link and direct into login page
  useEffect (
    () => {
      const urlLink = window.location.href
      setLoginPage (urlLink.split('?')[1])
    }
  ) 

  // Step 2 - Send link into login page
  const LinkLogin = (value) => {

    if (value === 'login') {
      //window.open ("https://2023.catalogonanet.com/?login","_self")
      window.open ("http://localhost:3000/?login","_self")
    } 

  }

  // Step 1 - Login button send to arrow function the step 2
  const returnPage = () => {

    if (loginPage == 'officenanet') {
      return ( 
        < OfficeNaNet />
      )
    } else if (loginPage == 'officeadmin') { 
        return ( 
          < OfficeAdmin />
        )
    } else if (loginPage == 'login') {
        return( 
          < LoginScreen />
        )
      } else {
          return (

            <C.mainContainer>
              
                <img 
                    src = {ImgMainWhite}
                />
        
                <button onClick = { () => LinkLogin('login') }>Click Here</button>
              
            </C.mainContainer>
      
          )        
        }

  }

  return (
    <>
      {returnPage()}
    </>
  )
}

export default Main
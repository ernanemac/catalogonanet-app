import React from 'react'
import * as C from './footer-style'

const Footer = () => {
  return (
    <C.footerContainer>
        Copyright © catalogonanet.com 2023. Todos os direitos reservados.
    </C.footerContainer>
  )
}

export default Footer
/*

Gray: #0e212c
Light gray: #8fb1b4

*/

import styled from "styled-components";

export const footerContainer = styled.div `
    text-align: center;
    height: 5vh;
    background-color: #0e212c;
    color: #8fb1b4;
    font-size: .8em;
    padding-top: 10px;
    padding-left: 20px;
`
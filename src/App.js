import React from 'react'
import { Routes, Route } from 'react-router-dom'
import Home from './pages/Home/home-index'
import Login from './pages/Login/login-index'
import OfficeAdmin from './pages/OfficeAdmin/officeadmin-index'
import Officenanet from './pages/Officenanet/officenanet-index'

function App() {
  return (
    <>
        <Routes>
          <Route path='/' element = {<Home/>}/>
          <Route exact path='/officenanet' element = {<Officenanet/>} /> 
          <Route exact path='/officeadmin' element = {<OfficeAdmin/>} />
          <Route path='/login' element = {<Login/>} />
          <Route path='*' element = {<Home/>} />
        </Routes>  
    </>
  );
}

export default App;

const express = require("express")
const app = express()
const mysql = require("mysql")
const cors = require('cors');

this.user = ''

const db = mysql.createPool({
    host: "localhost",
    user: "catal204_user",
    password: "Catalogoapp",
    database: "catal204_catalogodb",
})


// CORS

app.use(express.json())
app.use(cors())

const corsOptions = {
    origin: 'https://2023.catalogonanet.com',
    optionsSuccessStatus: 200
};
  
app.use(cors(corsOptions));

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// Query the database

app.post("/officeadmin", (req,res) => {
    res.send(this.user.charAt(0).toUpperCase() + this.user.substr(1))
})

app.post("/officenanet", (req,res) => {
    res.send(this.user.charAt(0).toUpperCase() + this.user.substr(1))
})

app.post("/login", (req,res) => {
    const nomenanet = req.body.nomenanet
    const password = req.body.password
    this.user = nomenanet
    
    db.query("SELECT * FROM usuarios WHERE nomenanet = ? AND senha = ? AND admins = 1", [nomenanet, password], (err, result) => {
        if (err) {
            res.send(err)
        }
        
        if (result.length > 0) {
            res.send({msg: "admin"})
        } else {
            db.query("SELECT * FROM usuarios WHERE nomenanet = ? AND senha = ? AND admins = 2", [nomenanet, password], (err, result) => {
                if (err) {
                    res.send(err)
                }
                
                if (result.length > 0) {
                    res.send({msg: "user"})
                } else {
                    res.send({msg: "User is not found"})
                }
            })
        }
    })

}) 
